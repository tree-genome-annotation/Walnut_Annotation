# Plant Computational Genomics Lab - UConn
# Alex Trouern-Trend - Wed Nov 20 11:52:02 EST 2019

# Identifies every .fa file in a directory and calculates minimum seq length

import os
import glob
from Bio import SeqIO

g = input("Would you like the results, including sequence ids, exported to a table? (Y/N) :")

for fasta in glob.glob('*.fa'):
    with open(fasta) as handle:
        minlen = 9999999
        smallest = []
        for record in SeqIO.parse(handle, "fasta"):
            if len(record.seq) < minlen:
                minlen = len(record.seq)
                smallest = []
                smallest.append(record.id)
            elif len(record.seq) == minlen:
                smallest.append(record.id)
    handle.close()
    print(fasta + "\t" + str(minlen))
    if g == "T":
        with open("minlentab.tsv", "a+") as tb:
            liner = fasta + "\t" + str(minlen) + "\t" + ",".join(smallest)
            tb.write(liner)
        tb.close()
        


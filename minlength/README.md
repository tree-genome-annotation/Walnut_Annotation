## Length Filtering

---

**Inputs**: softmasked genome files in fasta format

**Outputs**: Filtered AND softmasked genome files in fasta format.

---

The next step is to filter the genome down to include only contigs 3000 nucleotides in length or greater. This process may be skipped or the length of the filter may be adjusted.

This can be done simply using the [bioawk utility](https://github.com/lh3/bioawk)

Command to remove sequences less than 3000 basepairs:

---

`bioawk -c fastx '{ if(length($seq) >= 3000) { print ">"$name; print $seq }}' some_seq.fasta > some_seq.filtered.fasta`

---

Run [`lencheck.py`](./lencheck.py) to check for successful filtering.

This python script scans all fasta files in the folder and reports their minimum sequence length.


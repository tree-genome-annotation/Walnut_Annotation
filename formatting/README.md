## Final Preparation

1. Make final peptide & CDS fasta files (Coding sequence | Genes without introns):
	- Concatenate the two peptide sequence outfiles from the entap monoexonic and multiexonic outputs. `cat ./path/to/multi/final_annotated.faa ./path/to/mono/final_annotated.faa > filename.faa`
	- Make Retroelement free final amino acid file: use this concatenated peptide file and the `retrodomainGenes.txt` produced in the previous step as input into **remretros.py**. This removes the sequences that were classified as retroelements.
	- generate a final gene list from the final amino acid file using the following bash commands:
		- `grep -c ">" final_amino.faa > final_genelist.txt` (takes only headers)
		- `sed -i 's/>//g' final_genelist.txt` (removes the ">" symbol)
	- from the gFACs mono and multi runs, concatenate the genes_without_introns.fasta files: `cat path/to/mono/genes_without_introns.fasta path/to/multi/genes_without_introns.fasta`
	- with `final_genelist.txt` and the concatenated `genes_without_introns.fasta` file use **grabfiles.py** to generate the final CDS file
2. Make the final gtf using the concatenated `out.gtf` files from the monoexonic and the multiexonic runs and final_genelist.txt as input into the script, **createGFF.py**.
3. Make the final functional annotation.tsv using inverted grep with retrodomainGenes.txt list input on the concatenated mono and multi entap annotation file produced using **get_subSeq.py** in the previous step: `grep -v -f retrodomainGenes.txt cat_annotation_file.tsv > final_entap_annotation.tsv`

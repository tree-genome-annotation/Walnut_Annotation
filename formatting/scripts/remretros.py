from Bio import SeqIO

fasta_file = "East_proteome.faa" # Input fasta file
retro_file = "retrodomainGenes.txt"# Input interesting sequence IDs, one per line
result_file = "East_noRetros.faa" # Output fasta file

retros = set()
with open(retro_file) as f:
    for line in f:
        line = line.strip()
        if line != "":
            retros.add(line)

fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')
with open(result_file, "w") as f:
    for seq in fasta_sequences:
        if seq.id not in retros:
            SeqIO.write([seq], f, "fasta")

import sys
import linecache
import re
import os

# This script takes two arguments:
#    1. gff3 to be subset
#    2. gene headers, one per line

from itertools import tee, islice, izip_longest
def get_next(some_iterable, window=1):
    items, nexts = tee(some_iterable, 2)
    nexts = islice(nexts, window, None)
    return izip_longest(items, nexts)

#filename10 = '/labs/Wegrzyn/WalnutGenomes/results/create_gff3/gff3' + '/' + 'Jure.gff3'
filename10 = sys.argv[1]
lines = [line.rstrip('\n') for line in open(filename10)]

generow = []
endgfflines = []
gfflines = []
genes = []


def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

numlines = file_len(filename10)

#### create list of complete genes to parse the gff file with
i=1

for line, next_line in get_next(lines):
   row2 = line.split("\t")
   if i < numlines:
       nextrow = next_line.split("\t")
   if len(row2) == 9:
       if row2[2] == "gene":
           gr = i
           strand = row2[6]
           #print("found gene!")
       elif row2[2] == "mRNA":
           #print("found mRNA!")
           gfflines.append(i)
           generow.append(gr)
           gname = re.search('(?<=ID=)g[0-9]*\.t[0-9]', row2[8])
           #print(gname.group(0))
           seneg = gname.group(0)
           genes.append(seneg)
       elif nextrow[2] == "gene": 
           endgfflines.append(i)
           #print(nextrow[2])
       elif nextrow[2] == "mRNA":
           endgfflines.append(i)
       elif strand == "+":
           if "stop_codon" in row2[2]:
               endgfflines.append(i)
       elif strand == "-":
           if "start_codon" in row2[2]:
               endgfflines.append(i)
       elif nextrow[2] == "gene":
           endgfflines.append(i)
   i+=1

with open(sys.argv[2]) as p:
    genenames = p.readlines()

genematch = []

for line in genenames:
    line = line.strip()
    genematch.append(line)

print("You are looking for " + str(len(genematch)) + " genes!")

print("generow = "+ str(len(generow)))
print("gfflines = "+ str(len(gfflines)))
print("endgfflines = "+ str(len(endgfflines)))
print("genes = "+ str(len(genes)))

found = 0

for name in genes:
    for match in genematch:
        if name == match:
            found += 1
            #print(name)
            k=genes.index(name)
            l=generow[k]
            genehead=linecache.getline(filename10,l)
            print(genehead)
            entry1=gfflines[k]
            entry2=endgfflines[k]
            #prino( "# start gene " + name)
            for m in range(entry1,entry2+1):
                n=linecache.getline(filename10,m)
                print(n)
            #print("# end gene " + name)
            if found == len(genematch):
                print(found)
                print(len(genematch))

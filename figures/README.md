## Figures inputs and exections.  
A sample of input data for each figure is provided, where possible. Sample files are in the `Sample_data` subfolder.

figures can be viewed in published paper: [https://doi.org/10.1111/tpj.14630](https://doi.org/10.1111/tpj.14630). Many scientific articles are available for free on Library Genesis. You can also email alexander.trouern-trend@uconn.edu for a copy.

### Figure 1  
This figure was not made using scripts.

### Figure 2  
This figure describes the percentage of different types of BUSCOs in the species annotations. The inputs are BUSCO numbers, which are recorded directly in the script.

### Figures 3A and 3B  
Figure 3A describes walnut species and which orthogroups they belong to. This type of plot is known as an upset plot, where the dots below each bar indicate membership. The script for this plot is contained here, but the coloring was done after the plot was generated. The source data is:

|	|proteome.juct|	proteome.juhi|	proteome.jumi|	proteome.juni|	proteome.jure|	proteome.jusi|	proteome.ptst|	Total|
|---|-------------|--------------|---------------|---------------|---------------|---------------|---------------|--------|
|OG0000000|	4|	17|	27|	7|	59|	3|	73|	190|
 

Figure 3B is a cladogram, with each histogram indicating the number of genes belonging to particular orthogroups. Both the histogram and tree data is included in the script.

### Figure 4  
A line graph of substituion rates for those coding genes syntenic between J. hindsii and P. stenoptera. Data is entered in this form for the different measurements (see fig4 script, \*_awk for KN substitution, \*ks for KN substitution):  

|JcatxPtse_awk.txt|
|-|
|0.1166|
|0.0707|
|0.0714|
|0.1932|
|0.0402|
|0.0412|
|0.0547|
|0.0197|
|0.0552|

|JcatxPtse_kn.txt|
|-|
|0.0532|	
|0.0147|	
|0.0175|	
|0.0731|	
|0.0416|	
|0.0131|	
|0.0224|	
|0.0094|	
|0.0150|


### Figure 5
This figure was not made using scripts.


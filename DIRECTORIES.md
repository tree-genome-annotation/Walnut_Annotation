
🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳🌳  

Welcome to the directory list for the Walnut Annotation Project - what are you looking for?

You can find other information about this project at the following links
1. [GoogleSheet](https://docs.google.com/spreadsheets/d/1VOdAiVIfpvD68iL38zGpAwQ87giulllZENj6wJUxj9Y/edit#gid=128799542)
2. [GitLab](https://gitlab.com/tree-genome-annotation/Walnut_Annotation)
3. [Asana](https://app.asana.com/0/251398148685769/69073796409994)


## Directory Structure on the Xanadu server at UConn Health Center
The directories are structured to be inclusive of all the data and processes accumulated over the course of this project, but keeps the information relevant to the standard user separated from the background of trial, error and thousands of intermediate files. **The parent directory to this project is /UCHC/LABS/Wegrzyn/WalnutGenomes.** Comments/Questions can be sent to Alex at alexander.trouern-trend@uconn.edu.  
** 4 letter species codes: ** 
 
Code | Species | Common Name  
-----|---------|------------  
juct | *Juglans cathayensis* | Chinese Walnut
juhi | *Juglans hindsii* | Hinds Black Walnut
jumi | *Juglans microcarpa* | Texas Walnut
juni | *Juglans nigra* | Eastern Black Walnut
jure | *Juglans regia* | Persian (English) Walnut
jusi | *Juglans sigillata* | Iron Walnut
ptst | *Pterocarya stenoptera* | Chinese Wingnut

### `./data`
Houses the files obtained at the beginning of this project and repeat libraries.  
#### `./data/genomes/`
Two subdirectories exist, `./data/genomes/original` houses the genomes as they were received from University of California San Diego. `./data/genomes/softmasked` holds the genomes that have repetitive content softmasked and their index files for TopHat2 alignments (softmasking and alignment both descibed in README).
#### `./data/repeats/`
Holds fasta file for each genome that contains repeated sequences found using repeatmodeler. These are distinguished by 4-letter species codes listed in the table above. The single subdirectory `./data/repeats/robins/` contains some fasta files from the 5 repeat modeler runs completed by Robin Paul (juct, juhi, jumi, jure, ptst). It also holds some files that were appended to fasta files of repeat sequences fromodel organisms (contain significantly more sequences). **Some files from repeatmodeler runs are missing.**
#### `./data/transcriptomic/`
Transcriptome libraries from *Juglans regia* were used as evidence in the annotation process. 19 libraries taken from a variety of tissues exist as paired end fastq files within directories labeled by their library code. Information on tissue source, genotype, developmental stage, harvest date and additional stats can be gound in the Walnut project GoogleSheet linked above.

### `./processing`
Here is the bulk of all files associated with this project. It is organized by scripts used for processing, files from processes that were redone (learning) and the final set of files from the genome processing stage of this project (success). Processing encompasses genome masking, contig filtering, mapping of transcriptomic evidence, running BRAKER2 (gene prediction pipeline), gene model filtering and retroelement removal.
#### `./processing/scripts/`
The scripts are binned into two subdirectories based on whether they were used before or after the gene prediction step. `./processing/scripts/annotation/` holds scripts that were used before running the BRAKER2 gene prediction step whereas `./processing/scripts/filtering` bears scripts that were used for the gene model filtering process after post gene prediction.
#### `./processing/learning/`
As noted above, this directory holds most of the files generated in the trial and error (mostly error) process. There are 7 subdirectories that are each specific to a single species used in the study. `./processing/learning/Final_FTPfiles_06_18/` is a set of *final* files that still contained retroelements and had incorrectly labeled gene models(organized by species). The subdirs of each species folder (at `./adjusted/` if the root is the species folder) contains the same files with renamed gene models. These still contain retroelement gene models and are not labeled on a basis of 1 genemodel per count. `./processing/learning/ReAnno_Final/` is a similar to the Final_FTPfiles_06_18 subdirectory.
###### `./processing/learning/old_or_adjacent_Walnut-Runs`
This is a dump of Walnut Project ancient history. *Organization in Progress*
#### **`./processing/success/`**
This is a key directory. It contains the files and scripts (`./processing/success/scripts/`) involved in the processing that produced those final annotation files that are available on the TreeGenes FTP database ([TreeGenes](https://treegenesdb.org/FTP/)). The organization within each species dedicated subdirectory is a chimera of file type and step in the process. Details below using species specific subdirectory as root and $ to indicate species specific prefix.
###### `./annotation/`
Three files. $_complete_remRet.gtf is fully filtered structural annotation, $_complete_multi_mono.gtf is identical, but also contains genes bearing retroelement domains. $_functional_annotation.tsv is a functional annotation table produced by entap before identifying genes with retroelement domains. Contains annotations of all final gene models and also those removed for having retro domains. 
###### `./association_tables/`
The names of gene models were changed as part of the final preparation of files before uploading to FTP, these are the intermediate tables bearing old and new names. It is worth noting that there were several cases where the wrong prefix was in gene model name changes. Some of the *name tables* reflect those changes.
###### `./filtering/`
The gene prediction pipeline, BRAKER2 gives a high rate of false positives. The gene model sets were filtered using structural features (gFACs) and functional annotation (EnTAP) to identify most probable candidate genes (contained start/stop codons, had known protein motif). Monoexonic genes and multiexonic genes were split into two files and subjected to different sets of filters. `./filtering/mono_out` contains results from EnTAP and gFACs runs on monoexonic genes and `./filtering/multi_out` contains the same data for multiexonic genes.
###### `./retro_removal/`
InterProScan was used to identify genes with retroelement domains. The resulting .gff3 file lives in this folder. retrodomains.txt is a subset of that .gff3 that contains only genes with retroelement domains. retrodomainGenes.txt are the IDs of those genes.
###### `./sequence/`
This directory is contains fasta files of scaffold length-filtered genomes and several intermediate nucleotide and amino acid fasta files from the filtering process.
=======
> #### `./data/genomes/`
> Two subdirectories exist, `./data/genomes/original` houses the genomes as they were received from University of California San Diego. `./data/genomes/softmasked` holds the genomes that have repetitive content softmasked and their index files for TopHat2 alignments (softmasking and alignment both descibed in README).
> #### `./data/repeats/`
> Holds fasta file for each genome that contains repeated sequences found using repeatmodeler. These are distinguished by 4-letter species codes listed in the table above. The single subdirectory `./data/repeats/robins/` contains some fasta files from the 5 repeat modeler runs completed by Robin Paul (juct, juhi, jumi, jure, ptst). It also holds some files that were appended to fasta files of repeat sequences fromodel organisms (contain significantly more sequences). **Some files from repeatmodeler runs are missing.**
> #### `./data/transcriptomic/`
> Transcriptome libraries from *Juglans regia* were used as evidence in the annotation process. 19 libraries taken from a variety of tissues exist as paired end fastq files within directories labeled by their library code. Information on tissue source, genotype, developmental stage, harvest date and additional stats can be gound in the Walnut project GoogleSheet linked above.

### `./processing`
Here is the bulk of all files associated with this project. It is organized by scripts used for processing, files from processes that were redone (learning) and the final set of files from the genome processing stage of this project (success). Processing encompasses genome masking, contig filtering, mapping of transcriptomic evidence, running BRAKER2 (gene prediction pipeline), gene model filtering and retroelement removal.
> #### `./processing/scripts/`
> The scripts are binned into two subdirectories based on whether they were used before or after the gene prediction step. `./processing/scripts/annotation/` holds scripts that were used before running the BRAKER2 gene prediction step whereas `./processing/scripts/filtering` bears scripts that were used for the gene model filtering process after post gene prediction.
> #### `./processing/learning/`
> As noted above, this directory holds most of the files generated in the trial and error (mostly error) process. There are 7 subdirectories that are each specific to a single species used in the study. `./processing/learning/Final_FTPfiles_06_18/` is a set of *final* files that still contained retroelements and had incorrectly labeled gene models(organized by species). The subdirs of each species folder (at `./adjusted/` if the root is the species folder) contains the same files with renamed gene models. These still contain retroelement gene models and are not labeled on a basis of 1 genemodel per count. `./processing/learning/ReAnno_Final/` is a similar to the Final_FTPfiles_06_18 subdirectory.

> ###### <p> `./processing/learning/old_or_adjacent_Walnut-Runs` </p>
> <p> This is a dump of Walnut Project ancient history. *Organization in Progress* </p>

> #### `./processing/success/`
> This is a key directory. It contains the files and scripts (`./processing/success/scripts/`) involved in the processing that produced those final annotation files that are available on the TreeGenes FTP database ([TreeGenes](https://treegenesdb.org/FTP/)). The organization within each species dedicated subdirectory is a chimera of file type and step in the process. Details below using species specific subdirectory as root and $ to indicate species specific prefix.

> ###### <p> `./annotation/` </p>
> <p> Three files. $_complete_remRet.gtf is fully filtered structural annotation, $_complete_multi_mono.gtf is identical, but also contains genes bearing retroelement domains. $_functional_annotation.tsv is a functional annotation table produced by entap before identifying genes with retroelement domains. Contains annotations of all final gene models and also those removed for having retro domains. </p>

> ###### <p>`./association_tables/`</p>
> <p> The names of gene models were changed as part of the final preparation of files before uploading to FTP, these are the intermediate tables bearing old and new names. It is worth noting that there were several cases where the wrong prefix was in gene model name changes. Some of the *name tables* reflect those changes. </p>

> ###### <p> `./filtering/` </p>
> <p> The gene prediction pipeline, BRAKER2 gives a high rate of false positives. The gene model sets were filtered using structural features (gFACs) and functional annotation (EnTAP) to identify most probable candidate genes (contained start/stop codons, had known protein motif). Monoexonic genes and multiexonic genes were split into two files and subjected to different sets of filters. `./filtering/mono_out` contains results from EnTAP and gFACs runs on monoexonic genes and `./filtering/multi_out` contains the same data for multiexonic genes. </p>

> ###### <p> `./retro_removal/` </p>
> <p> InterProScan was used to identify genes with retroelement domains. The resulting .gff3 file lives in this folder. retrodomains.txt is a subset of that .gff3 that contains only genes with retroelement domains. retrodomainGenes.txt are the IDs of those genes. </p>

> ###### <p> `./sequence/` </p>
> <p> This directory is contains fasta files of scaffold length-filtered genomes and several intermediate nucleotide and amino acid fasta files from the filtering process. </p>

### `./results`
For each species, a subdirectory containing annotation files described in the table below. `./sequence/structural_stats` contains the results of gFACs run against the final set of genemodels. `./sequence/benchmarks` contains two subdirectories, one for the results of busco run on the final annotation files and the other for the results of running quast on the final annotation files.

**Final Annotation Filetypes**

File Suffix | Description
----------- | -----------
cds.fa | Coding regions of each gene model (exons without introns)
funct.tsv | Functional annotation of each gene model against RefSeq, Plant RefSeq and Uniprot databases. Constructed with EnTAP.
.gtf | Structural annotation of each gene model. Constructed using gFACs.
.peptides.fa | Translated amino acid sequence of each gene model.
.repeats.fa | Fasta containing repetitive content from genome. Discovered using repeatmodeler. These sequences are softmasked (lowercase) in the genome file.

### `./analyses`
Files associated with the comparative analysis using the new genome annotations are organized into sudirectories based on the analysis. `./analyses/orthofinder` contains three orthologous group analyses using orthofinder. **juglans** compares only species in the genus juglans. **juglandaceae** contains *Juglans* and the outgroup, *Pterocarya stenoptera*. **Viridiplantae** compares those 7 *Juglandaceae* genomes with genomes from 15 other species in embryophyta. `./analyses/cafe` contains analyses of gene family evolution rates based on th OrthoFinder results.

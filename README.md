**Walnut Annotation Pipeline**

Here we presemt the details of a pipeline used to annotate the reference genomes of 6 _Juglans_ (walnuts) species and one additional species from its sister genus, _Pterocarya_ (wingnuts). All computations were performed on a SLURM-based cluster. Scripts are sorted according to the major steps of the genome assembly process, see [`README.md`](./) for ordered outline. Each folder also features a separate markdown (`.md`) file that describes the process and rationale in greater detail. This document serves as an overview of that process and a resource for code useful to those embarking on eukaryotic genome annotations.

published paper: [https://doi.org/10.1111/tpj.14630](https://doi.org/10.1111/tpj.14630). Many scientific articles are available for free on Library Genesis. You can also email alexander.trouern-trend@uconn.edu for a copy.

### Abbreviations/Bioproject
Species are typically abbreviated in the following way, along with their Genbank biosample identifiers. The bioproject is [`PRJNA445704`](https://www.ncbi.nlm.nih.gov/bioproject/1389384744):  

| Species name | Common name | 4-letter code | Biosample |
| ------ | ------ | ------ | ------ |
| *Juglans cathayensis* | Chinese walnut | Juct | [SAMN08795866](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795866/) |  
| *Juglans hindsii* | Northern California black walnut | Juhi | [SAMN08795864](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795864) |  
| *Juglans microcarpa* | Texas black walnut | Jumi | [SAMN08795863](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795863) |  
| *Juglans nigra* | Eastern black walnut | Juni | [SAMN08795865](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795865) |  
| *Juglans regia* | Persian walnut (English walnut) | Jure | [SAMN08795861](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795861) |  
| *Juglans sigillata* | Iron walnut | Jusi | [SAMN08795862](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795862) |  
| *Pterocarya stenoptera* | Chinese wingnut | Ptst | [SAMN08795867](https://www.ncbi.nlm.nih.gov/biosample/SAMN08795867) |  

### FTP File repository
Final gene annotations for each species, including genome and annotation files, are available to download in the following locations:  

*  [*Juglans cathayensis*](https://treegenesdb.org/FTP/Genomes/Juct/v1.0/)
*  [*Juglans hindsii*](https://treegenesdb.org/FTP/Genomes/Juhi/v1.0/)
*  [*Juglans microcarpa*](https://treegenesdb.org/FTP/Genomes/Jumi/v1.0/)
*  [*Juglans nigra*](https://treegenesdb.org/FTP/Genomes/Juni/v1.0/)
*  [*Juglans regia*](https://treegenesdb.org/FTP/Genomes/Jure/v1.4/)
*  [*Juglans sigillata*](https://treegenesdb.org/FTP/Genomes/Jusi/v1.0/)
*  [*Pterocarya stenoptera*](https://treegenesdb.org/FTP/Genomes/Ptst/v1.0/)

# Process
---
### [Repeat Masking](./repeatmasker)

---

Genome assemblies used in this project were published in 2018 by [_Stevens et al._](https://doi.org/10.1534/g3.118.200030) Genome assemblies were used to build repeat libraries ([*RepeatModeler*](http://www.repeatmasker.org/RepeatModeler/)) and then softmasked ([*RepeatMasker*](http://www.repeatmasker.org/)) using only the native repeat libraries. 

**Note:** Genome masking converts repetitive elements and low complexity regions of the genome to different characters, which helps inform downstream software. Two types of masking exist: *hardmasking* and *softmasking*. Hardmasking removes data by converting repetitive sequences to null value `X`. For example, repeated `GATA` may become a series of `X`. Softmasking, on the other hand, preserves the data by converting repeat  elements to lowercase nucleotides, which can then be used later in in separate analyses.

---

### [Minimum Length Filtering](./minlength)

---

Minimum contig length was set at 3000 nt in this case. The appropriate min length cutoff for other genomes will depend on the sequencing depth and genome contiguity.

Bioawk was used to remove contigs of lengths < 3000 nucleotides from the genome assembly .fasta file for each species. [`lencheck.py`](./minlength/lencheck.py) was used to check the minimum sequence length of each assembly. The sequence data contains nucleotids (`A`, `T`, `C`, `G`), softmasked nucleotides (`a`, `t`, `c`, `g`), and `N`, which indicates an unidentified nucleotide (in this case, either `A`,`T`,`C` or `G`). `N`'s in the dataset are the result of an unsuccessful basecall and are normal.

---

### [Evidence Alignment](./tophat)

---

Quality-Controlled transcriptomic datasets (RNAseq reads) were used as evidence to guide gene prediction using BRAKER2. [*BowTie2*](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml) indices were generated ([`bowtiebuild.sh`](./tophat/bowtiebuild.sh)) to use for [*TopHat*](https://ccb.jhu.edu/software/tophat/manual.shtml) read mapping ([`tophat.sh`](./tophat/tophat.sh)) of transcriptome libraries to softmasked genomes.

For each library, *Tophat* produces a separate bam file, which are [sorted](./tophat/samtoolsort.sh) and [merged](./tophat/mergetools.sh), using [*bamtools*](https://wiki.rc.usf.edu/index.php/BamTools). Indices in the same folder as the masked and filtered genome.

---

### [Structural Annotation](./braker)

---

Using the filtered, masked genome and the merged bam file, we can run [*BRAKER2*](https://doi.org/10.1093/bioinformatics/btv661), a semi-unsupervised gene prediction pipeline, to predict gene models from the genome. BRAKER2 uses RNAseq mapping data as evidence to identify genes in the genome. It then learns from these structures to predict additional genes without evidence. This is the rate-limiting step of the genome annotation pipeline.

BRAKER2 produces a few files important for the downstream analyses. `augustus.*` files, including `gff3`, `gtf`, and `aa`.

---

### [Gene Model Selection](./filtering)

---

[*gFACs*](https://readthedocs.org/projects/gfacs/) was used to split the monoexonic (with a single exon) and multiexonic (with at least one intron) genes from one another. *gFACs* also reports statistics on set of gene models from *BRAKER2*.

Using custom management scripts, the mono and multiexonics can be organized, filtered, and run through *EnTAP*

Multiexonic and monoexonic sets were filtered separately to exclude low-quality gene models. [*EnTAP*](https://entap.readthedocs.io/en/latest/) is a pipeline which functionally annotates input gene models using a set of wrapped tools. We leverage these annotations to improve the confidence of gene models and to identify and remove low-confidence gene models (those without sequence similarity hit or gene family assignment). For monoexonics, a custom database is referenced. For multiexonics, Uniprot and Plant RefSeq are used. Of the many files produced by EnTap, we move forward with the `final_annotations_lvl0_no_contam.tsv`. Other levels of output can be selected for more specific [Gene Ontology](http://geneontology.org/) term assignments.

---

### [Retroelements](./retroelements)

---

A .gff3 file with protein domain annotations is generated by reconfiguring EnTAP settings to leverage [InterProScan](https://github.com/ebi-pf-team/interproscan/wiki) and re-annotating the combined set of filter-passing multiexonic and monoexonic gene models. The gene list is then filtered to remove genemodels with common retroelement ontologies like "gag-polypeptide" and "reverse transcriptase".

---

### [Formatting files](./formatting)

---

A complete annotation should include translated gene models `Genes.aa`, the gene feature table `Genes.gtf`, and nucleotide gene models with and without introns `Genes.nt` + `Genes.cds.nt`. *gFACs* and helper scripts for file construction scripts are inluded. *EnTAP* `tsv` file and original *braker* files are the building blocks here.

---

### [Genome Statistics](./statistics)

---

[BUSCO](https://busco.ezlab.org/) and [QUAST](http://quast.sourceforge.net/quast.html) generated informative statistics using phylogenetic-based genome completeness estimates and structural assessment, respectively.

---


#[figures](./figures) contains the `.R` scripts and data used to generate figures in the publication.


---

### Packages used

---

A variety of packages and databases were used in this project, including the following:
*  [TopHat](https://ccb.jhu.edu/software/tophat/index.shtml)
*  [BRAKER2](https://github.com/Gaius-Augustus/BRAKER)
*  [EnTAP](https://gitlab.com/enTAP/EnTAP/-/tags)
*  [gFACs](https://gitlab.com/PlantGenomicsLab/gFACs)
*  [RepeatMasker](http://www.repeatmasker.org/)
*  [RepeatModeler](http://www.repeatmasker.org/RepeatModeler/)
*  [BowTie2](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml)
*  [bamtools](https://wiki.rc.usf.edu/index.php/BamTools)
*  [BUSCO](https://busco.ezlab.org/)
*  [QUAST](http://quast.sourceforge.net/quast.html)
*  [InterProScan](https://github.com/ebi-pf-team/interproscan/wiki)
*  [GeneOntology](http://geneontology.org/)
*  [RefSeq](https://www.ncbi.nlm.nih.gov/refseq/)
*  [Uniprot](https://www.uniprot.org/)

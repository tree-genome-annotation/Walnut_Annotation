### Building files based on entap and gFACs output

Directories housing entap outputs from the **CCmono.sh** and **CCmulti.sh** runs performed in step 5 will be named `monoentap_out` and `entap_out`, respectively and seated as subdirectories of the directories where the shell scripts were run. Those entap out directories contain the same suit of extensive information about similarity search results and gene ontology data. More information about enTap output can be found here: `https://entap.readthedocs.io/en/latest/`

The final annotation results are stored in a series of .tsv files that differ from one another based on two variables. Firstly, there are several different levels of GO (Gene Ontology) term normalization distributed across final annotation suffixes (lvl0, lvl3 and lvl4 are standard enTap outputs) level 0 (lvl0) outputs represent the broadest level of GO term reporting; all GO terms associated with Ontology results will be printed to the annotation file. There are two separate annotation files printed for each level of GO term reporting, one that prints the annotations associated with sequences labeled as contaminants (provided to enTap using the --contams flag, which is N/A for the sake of this genome annotation pipeline) and another that prints the annotations results for non-contaminant sequences.

For both the mono and multiexonic annotations we will use the files, `final_annotations_lvl0_no_contam.tsv` and extract all of the sequences that have been functionally annotated or assigned to gene families (functions of Diamond and eggNOG, respectively).

Using the available python script, **get_SubSeqs.py**, genes that passed all filters and were annotated by enTap can be extracted from the `final_annotations_lvl0_no_contam.tsv` files and a new file with only annotated genes (before retroelement removal) can be constructed.

### Prep GTF For Later Step

Construct a gtf file containing both the monoexonic and the multiexonic genemodels. This can be done with a simple cat command, but will result in two .gtf header lines being present in the same file.
    * The two files will be located in their respective directories where CCmono.sh and CCmulti.sh were run. Mono path: dir/mono_out/Mono_out.gtf | Multi path: dir/multi_out/Multi_out.gtf


### Leverage Interproscan Using Entap

For Multi + Mono set separately:
1. This is not a streamlined process at the moment, but it does work. Using that final annotationed geneset produced by the **get_SubSeqs.py** script, construct a list of headers.
    - `awk '{ print $1 }' final_annotations_from_script.tsv > list_of_headers.tsv`  <-- This command will do the trick.  
2. Use the **grabfiles.py** script with that headerlist to subset the augustus.aa amino acid multifasta given by Braker2.  
3. Make sure the number of sequences in your new amino acid multifasta subset is equivalent to the number of headers you used to retrieve sequences.  
4. Run entap again with additional parameters:  
	* --ontology 1 # Tells enTap to run interproscan instead of eggNOG
        * module load interproscan
	* --out-dir ./retros_out # new output directory so that the former enTap runs are not overwritten.
    This can be accomplished quickly by modifying a copy of the entap scripts written by CCmulti.sh and CCmono.sh

### Remove Retroelements

The interpro_results.gff3 files found in the retroelement entap run_out/_out/ontology/InterProScan directory can be concatenated then probable retroelements removed using the following process:
1. grep -i -e "gag-polypeptide" -e "retrotransposon" -e "reverse transcriptase" interpro.gff3 > retrodomains.txt 
    This step searches for retroelement protein domains in the interpro annotation.
2. cut -f1 retrodomains.txt | sort | uniq > retrodomainGenes.txt 
    Make a list of retro gene headers
3. python createGFF.py --gff multi_mono_annotated.gtf  --nameList retrodomainGenes.txt --out final_annotated_remRet.gtf 
    Use the available python script "createGFF.py" to subset the gtf file constructed in the `### Prep GTF For Later Step` section.

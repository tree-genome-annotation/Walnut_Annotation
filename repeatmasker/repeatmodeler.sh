#!/bin/bash
# Submission script for Xanadu
#SBATCH --job-name=rpmodel
#SBATCH -o repeatmodeler-%j.output
#SBATCH -e repeatmodeler-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=22
#SBATCH --partition=general

# Run the program                 

export DATADIR='/Genome/Location/'
export SEQFILE="Genome.fa"
export DATABASE="Spp_name"


WORKDIR=/scratch/$USER/repeatmodeler/$JOB_ID
mkdir -p $WORKDIR
cp $DATADIR/$SEQFILE $WORKDIR
cd $WORKDIR

# Note that modules listed here may not be up to date or present when you are trying to run this script
module load RepeatModeler/1.0.8
module load rmblastn/2.2.28

RepeatModeler -engine ncbi -pa 22 -database $DATABASE
rsync -a ./consensi.fa.classified $DATADIR/$SEQFILE.consensi.fa.classified


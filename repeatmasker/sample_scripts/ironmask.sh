#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=ironmask
#SBATCH -o repeatmasker-%j.output
#SBATCH -e repeatmasker-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=END
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general

# Run the program                 
# echo pita.v2.0.1.masked.5k.fasta"$SLURM_ARRAY_TASK_ID".fa

module load RepeatMasker/4.0.6

# not sure if -lib is necessaray
# -lib /tempdata3/sumaira/RepeatMasker/Loblolly/PitaRepeats.fasta

RepeatMasker IronWal.fa -dir /UCHC/LABS/Wegrzyn/WalnutGenomes/tmp_IronWal/ -gff -a -noisy -low -xsmall| tee output_ironwal.fa.txt


#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=IronRP
#SBATCH -o repeatmodeler-%j.output
#SBATCH -e repeatmodeler-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=himem4

# Run the program                 

export DATADIR='/UCHC/LABS/Wegrzyn/WalnutGenomes/IronWal'
export SEQFILE="IronWal.fa"
export DATABASE="IronWal"


WORKDIR=/scratch/$USER/repeatmodeler/$JOB_ID
mkdir -p $WORKDIR
cp $DATADIR/$SEQFILE $WORKDIR
cd $WORKDIR

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28

#BuildDatabase -name IronWal -engine ncbi IronWal.fa
nice -n 10 RepeatModeler -engine ncbi -pa 22 -database $DATABASE
rsync -a ./consensi.fa.classified $DATADIR/$SEQFILE.consensi.fa.classified


#!/bin/bash 
# Submission script for Xanadu 
#SBATCH --job-name=rpm 
#SBATCH -o repeatmasker-%j.output 
#SBATCH -e repeatmasker-%j.error 
#SBATCH --mail-user=your.email@domain.com 
#SBATCH --mail-type=END 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8 
#SBATCH --partition=general 
#SBATCH --array=1-500%20 

# Run the program                 
echo Species.fa"$SLURM_ARRAY_TASK_ID".fa 

module load RepeatMasker/4.0.6 

# The pipe "|" here creates an output text file that can be checked for errors
RepeatMasker genome.slice."$SLURM_ARRAY_TASK_ID".fa -dir /Output/Directory -lib /repeatMasker/consensi.fa -pa 4 -gff -a -noisy -low -xsmall| tee output_spp."$SLURM_ARRAY_TASK_ID".txt


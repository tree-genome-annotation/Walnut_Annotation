#!/bin/bash
#BATCH --job-name=samtoolmerg
#SBATCH -o merg_spp-%j.output
#SBATCH -e merg_spp-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=END
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=256
#SBATCH --cpus-per-task=16
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load samtools

cd /spp/collected_bams

samtools merge --threads 16 -b collected.txt spp_merged.bam

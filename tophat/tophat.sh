#!/bin/bash
#BATCH --job-name=tophat
#SBATCH -o tophat_I-%j.output
#SBATCH -e tophat_I-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=END
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=500G
#SBATCH --cpus-per-task=32
#SBATCH --partition=himem1,himem3,himem4,himem5

module load bowtie2/2.2.9
module load tophat/2.1.1

#In our case, we had five collections of files: libAA through libAE
#Each was put into a separate script to distribute processing
cd /libAA/
for d in */ ; do
    mkdir -p ~/output/"$d"/tophat_out
    mkdir -p ~/output/"$d"/tophat_out/tmp
    export TMPDIR="~/output/"$d"/tophat_out/tmp" 
    echo $TMPDIR
    cd $d
    tophat -p 32 --library-type fr-firststrand --output-dir ~/output/$d/tophat_out spp_filtered_genome.fa /libAA/${d}trimmed_output_file1_${d%/}.fastq /libAA/${d}trimmed_output_file2_${d%/}.fastq 
    cd ../
done 

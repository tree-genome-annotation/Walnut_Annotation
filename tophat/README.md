## Short Read Alignments

**Inputs**: Transcriptome fastq files, bowtie indices  
**Outputs**: Aligned .bam files

---

Now that the genome is masked for repeated elements, we move onto aligning it to our transcriptome evidence. In our case, the transcriptome evidence exists as a number of libraries sorted into five folders, labelled A through E. For tophat inputs we want:
* Fastqs with transcriptome information
* Bowtie indices present in the tophat working directory (from bowtiebuild.sh)
* gmap indices present in tophat working directory (from gmapBuild.sh)

---
### SRA: Biosamples from *J. regia* Transcriptome Bioproject

22 libraries were used, representing 19 tissue types. For XYZ, two libraries were combined into a single read set for alignment.

| Tissue Type | Tissue Code | Developmental Stage | Source | Total Reads | Reads Post-QC | Accession |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| catkins | CK | immature | tree | 39,254,844 | 37,113,012 | SRX426484 or SRX404315|
| hull dehiscing | HU | senescent | tree | 61,885,858 | 54,850,389 | SRX404329 |
| embryo, mature | EM | mature | tree | 37,247,600 | 34,387,782 | SRX404328 |
| pellicle | PL | mature | tree | 43,392,812 | 41,426,900 | SRX404327 |
| packing tissue, mature | PK |  mature | tree | 59,283,694 | 49,505,694 | SRX404326 |
| hull cortex | HC | mature | tree | 64,446,528 | 61,424,051 | SRX404325 |
| hull peel | HP | mature | tree | 44,029,546 | 42,804,402 | SRX404324 |
| packing tissue, immature | PT | immature | tree | 64,903,726 | 61,797,713 |
| hull immature | HL1 | immature | tree | 62,364,320 | 59,066,242 | ??? |
| hull immature | HL6 | immature | tree | 57,673,738 | 54,040,229 | SRX404322 |
| fruit immature | IF | immature | tree | 58,683,826 | 55,371,217 | SRX404322 | 
| leaf, mature, apr08 | LE | vegetative | tree | 61,822,738 | 58,571,355 | SRX404320 |
| leaf, mature, jun08 | LM | vegetative | tree | 53,333,298 | 42,408,263 | SRX404319 |
| somatic embryo | SE | immature | in vitro | 28,357,550 | 27,180,861 | SRX404317 |
| pistillate flower | FL3 | vegetative | tree | 43,330,174 | 40,571,295 | SRX404104 (run: SRR1068421) |
| pistillate flower | FL6 | vegetative | tree | 33,983,772 | 29,328,010 | SRX404104 (run: SRR1068422) |
| callus exterior | CE | N/A | in vitro | 30,577,642 | 28,100,032 | SRX404102 | 
| callus interior | CI | N/A | in vitro | 60,902,168 | 58,222,286 | SRX404099 |
| root | RT | vegetative | pot | 39,839,020 | 37,852,140 | SRX404094 |
| leaf, young | LY2 | vegetative | tree | 43,732,392 | 40,809,417 | SRX403877 |
| leaf, young | LY7 | vegetative | tree | 32,326,356 | 24,361,590 | SRX426483 |
| vegetative bud | VB | vegetative | tree | 41,466,970 | 38,936,041 | SRX403874 |

---
#### Genome indexing

Tophat utilizes BowTie2 genome indices to map transcriptomic reads. The script **bowtiebuild.sh** was used with the genome-to-be-indexed as input. These supporting genome indices should be present in the same directory as the genome fasta file as referenced in the tophat script **tophat.sh**.

Those indices can be created with the scripts provided here. Running and bowtiebuild are relatively simple, point at the input genome and point at the output directory or file.

---

#### Read mapping

Once the indices are present, edit the tophat script to point at the transcriptome evidence in line 24, then your output directory in 27, 28, and 29. Run tophat.sh. 

After tophat has completed, the resulting `.bam` files (one per transcriptome library)can be sorted using `samtoolssort.sh` and then combined with `mergetools.sh`.

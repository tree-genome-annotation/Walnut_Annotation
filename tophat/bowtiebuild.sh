#!/bin/bash
# Submission script for Xanadu
#SBATCH --job-name=sppbowtie
#SBATCH -o spptie-%j.output
#SBATCH -e spptie-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=END
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task 8
#SBATCH --partition=general

module load bowtie2/2.3.3.1

bowtie2-build spp.fa.masked.filtered spp_bowtie
#spp.fa.masked.filter -> input
#spp.fa.masked.filter -> output, may need to match genome name

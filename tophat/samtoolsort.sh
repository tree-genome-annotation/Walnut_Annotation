#!/bin/bash
#BATCH --job-name=samtoolsort
#SBATCH -o sort_spp-%j.output
#SBATCH -e sort_spp-%j.error
#SBATCH --mail-user=your.email@domain.com
#SBATCH --mail-type=END
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=200G
#SBATCH --cpus-per-task=32
#SBATCH --partition=general

module load samtools

cd /spp/collectedbams

for file in /spp/collectedbams/*.bam ; do
	samtools sort -o $file_sorted.bam --threads 32 $file
done


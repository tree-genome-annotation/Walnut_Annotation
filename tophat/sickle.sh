#!/bin/bash
#$ -N sickle_job
#$ -M anvin.thomas@uconn.edu
#$ -m besa
#$ -S /bin/bash
#$ -cwd
#$ -pe smp 6
#$ -o newsickle_$JOB_ID.out
#$ -e newsickle_$JOB_ID.err

sickle pe -f /archive/Reju_transcript_rawreads/raw_reads/CI/trimmed_output_file1_CI.fastq.bz2 -r /archive/Reju_transcript_rawreads/raw_reads/CI/trimmed_output_file2_CI.fast.bz2 -t sanger -o trimmed_newoutput_R1_CI.fastq -p trimmed_newoutput_R2_CI.fastq -q 33 -l 40 -n -s trimmed_singles_file.fastq
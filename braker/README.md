## Gene Prediction

**Input**: Filtered, softmasked genome, bam files from transcriptome  
**Output**: Output directory, most importantly multiple `augustus.*` files

---

#### BRAKER2 Setup

Local installation of BRAKER2 may be necessary.

BRAKER uses [Augustus](https://github.com/Gaius-Augustus/Augustus) to find and structurally annotated the genes present in the given genome assembly. [GeneMark-ET](http://exon.gatech.edu/GeneMark/) is also used for gene prediction.

Due to some licensing issues and server software versions, braker cannot run successfully with normal `module load` commands for all of the dependencies. In this case Perl5, braker, and Augustus are installed in the home directory. See the Braker/Augustus user guide ([here](https://github.com/Gaius-Augustus/BRAKER)) for more information on installation.

---

#### Run BRAKER2

With dependencies set up, the filtered/softmasked genome and sorted/merged bam are inputs. Importantly `--softmasking 1` is set.

See example script, `**braker.sh**`.

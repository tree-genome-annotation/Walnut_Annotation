#!/bin/bash
# Submission script for Xanadu
#SBATCH --job-name=braker_
#SBATCH -o braker-%j.output
#SBATCH -e braker-%j.error
#SBATCH --mail-user=your.name@email.com
#SBATCH --mail-type=END
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --mem=100G
#SBATCH --partition=general

# Dependencies and paths on Xanadu

module load perl/5.24.0
export PERL5LIB=/home/CAM/example_dir/perl5/lib/perl5/
module load bamtools/2.4.1
export PATH=/home/CAM/example_dir/augustus-3.2.3/bin:/home/CAM/example_dir/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config

# Running BRAKER

/home/CAM/example_dir/2.0/braker.pl \
--cores 32 \
--genome=/spp/spp.genome.softmasked.filt.fa \
--species=Spp_braker2.0 \
--bam=/spp/spp_merged.bam \
--GENEMARK_PATH=/home/CAM/example_dir/gm_et_linux_64/gmes_petap/ \
--BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.4.1/bin/ \
--workingdir=/output/directory/ \
--gff3 --softmasking 1



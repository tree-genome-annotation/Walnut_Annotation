#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=64G
#SBATCH --mail-user=your.email@example.com
#SBATCH -o q_%j.out
#SBATCH -e q_%j.err

module load quast

quast.py -o ~/Quast/out -R  ~/Species1/genome/Spp_one.1_0.fa -e -G ~/Species1/annotation/Spp_one.1_0.gtf -t 8 ~/Species1/annotation/Spp_one.1_0.cds.fa


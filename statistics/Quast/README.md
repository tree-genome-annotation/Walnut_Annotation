**Input**: Genome file in fasta, coding sequence in fasta, and a .gtf file

**Output**: Statistics based on those inputs!

---

Quast is a simple, one line command that can return useful statistics about a genome. It requires three files commonly associated with genome annotations. See example script **quast.sh**.

---

Quast can return some of the following information:
* Number of contigs (at various numbers of bp)
* Total length of assembly
* GC percentage
* N50, N75, L50, L75
* N\'s per 100 kbp


## Analyzing genomes and other annotation inputs and outputs

* [Quast](http://bioinf.spbau.ru/quast)
* [BUSCO](https://busco.ezlab.org/)

Quast and BUSCO are two packages we used to analyze our annotations. It can be helpful for quality assurance to recover general or specific statistics for the files throughout multiple points in the pipeline,

### Quast 
**Quast** is useful for reporting general statistics on genome assemblies, such as size and number of contigs or scaffolds at particular lengths. It uses genomic or coding region nucleotide fastas and .gtf mappings.

### BUSCO
**BUSCO** can estimate annotation completeness. Given the amino acid fasta (also, genomic fasta and mappings), it compare to a database of taxonomically related BUSCO genes.

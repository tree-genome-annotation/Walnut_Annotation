#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=64G
#SBATCH --mail-user=your.email@example.com
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err


module load busco/3.0.2b
module unload augustus
export PATH=~/augustus/bin:~/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config

#i = genome, l = lineage (we used embryopyta)
run_BUSCO.py -i ~/Spp_complete_multi_mono.aa -l /isg/shared/databases/busco_lineages/embryophyta_odb9/ -o Busco_cwal_genes_32 -m proteins -c 16 


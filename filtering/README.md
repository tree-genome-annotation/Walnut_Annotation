## Multiexonic and Monoexonic Gene Model Filtering

**Input**: Augustus files from braker run  
*  genome.fasta 
*  augustus.gff3 
*  augustus.aa  
  
**Output**: Completed EnTap run  

---

#### Dependencies

With braker completed, we will have a number of output files. Most importantly, in your output directory from the step 4, there will be about four `augustus.*` files. In order to simplify this process, `CC.sh` has been written to create and run the various scripts in this step. We will be passing two augustus files to both `CCmono.sh` and `CCmulti.sh`: `augustus.gff3` and `augustus.aa`.

It is also important to note that EnTAP is required, as well as a local folder to your files containing gFACS, which is available here: [https://gitlab.com/PlantGenomicsLab/gFACs](https://gitlab.com/PlantGenomicsLab/gFACs). EnTAP can be used as a module, or can be installed from here: [https://gitlab.com/enTAP/EnTAP](https://gitlab.com/enTAP/EnTAP).

This folder should be called "gFACs" and contains `gFACs.pl`.

These two scripts create other slurm scripts to be run by the server sequentially. If you need to look at the specific script components, you may find it easier to look in the `*_component_scripts` folder. These are the scripts that are output when the parent script is run.

You can monitor the job queue (`squeue`) to make sure things are progressing smoothly. Jobs will be launched with your username.

#### Monoexonics

As the header to `CCmono.sh` says, a gfacs folder mult exist in the directory that the script exists in. With that in place, simply run it with `sh CCmono.sh your_email genome_fasta augustus.gff3`.

The monoexonic script will run multiple jobs, as mentioned in the slurm queue above.
1. First it will use gFACs to filter the multiexonic reads OUT, while preserving monoexonics that meet certain filters.
2. It then extracts the headers to these filtered reads, and stores them in a text file
3. It then extracts the correct reads from the genome fasta
4. The genome is split into many pieces and Serial Interpro is used to analyze each piece at a time, retrieving amino acid info
5. With the filtered amino acid files, we can run EnTap (which takes in a dmnd index of proteins and the filtered amino acids)
6. EnTap will report annotation information (cool!) which we can extract

EnTap has many outputs, and we can use them to work on our final outputs in step 6!

#### Multiexonics

Same as monoexonics, a gfacs folder is expected in the same directory as the script. This input also requires the locations of `augustus.aa` along with email, genome, and augustus.gff3. 

Structured in the same way, the multiexonics has fewer steps total:
1. Multiexonics require an additional input: augustus.aa
2. It uses gfacs to filter out the _mono_exonic reads, and again filtering the results
3. The headers are again extracted and stored
4. And an amino acid fasta output is generated with the filtered information
5. At this point, we can again run EnTap to generate annotation information

With both EnTap runs completed, we can move to step six.

#!/bin/bash

#Usage: 'sh CCmulti.sh email genome_fasta augustus.gff3 augustus.aa'
# Input files are expected to be in the same directory as CCmulti.sh
# A gFACs folder must exist in the directory this script runs in, named "gFACs" and containing "gFACs.pl"

email=$1
fasta=$PWD/$2
gff3=$PWD/$3
amino=$PWD/$4

echo "Full input file paths:"
echo $fasta
echo $gff3
echo $amino

echo "Your email: $email"

mkdir multi_out
#### Create gFACs run file

gFACsFILE="./multi_gFACs.sh"
/bin/cat <<EOM >$gFACsFILE
#!/bin/bash
#BATCH --job-name=gFACs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --mem=32GB
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
#SBATCH --mail-type=END
#SBATCH --mail-user=$email
#SBATCH -o stats_%j.out
#SBATCH -e stats_%j.err

module load perl
cd gFACs/

perl gFACs.pl \\
-f braker_2.0_gff3 \\
--statistics \\
--statistics-at-every-step \\
-p Multi \\
--splice-rescue \\
--rem-start-introns \\
--rem-monoexonics \\
--rem-end-introns \\
--splice-table \\
--min-exon-size 20 \\
--min-intron-size 20 \\
--rem-genes-without-start-codon \\
--rem-genes-without-stop-codon \\
--get-fasta-without-introns \\
--get-fasta-with-introns \\
--create-gtf \\
--fasta $fasta \\
-O ../multi_out \\
$gff3

cd ../
EOM
echo "file: multi_gFACs.sh created"

## use headers with grabfiles.py and completeMultiExonics.fasta file
#### must create a bash file so it can be run with slurm job_id dependencies
/bin/cat <<EOM >"./multi_grep_headers.sh"
#!/bin/bash
#SBATCH --job-name=grep
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grep_%j.out
#SBATCH -e grep_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

grep ">" multi_out/Multi_genes_with_introns.fasta | grep -oE "g.*"  > multi_headers.txt
sed -i -e 's/\(g[[:digit:]]*\)\.\([[:digit:]]*\)/\1\.t\2/g' multi_headers.txt
sed -i -e 's/\(g[[:digit:]]*\)$/\1.t1/g' multi_headers.txt
EOM
echo "file: multi_grep_headers.sh created"

#### Pipe grep into grep to omit leading `>` character

### Create grabfiles.py and assign inputs

grabpy="./grabfiles.py"
/bin/cat <<EOM >$grabpy
from Bio import SeqIO

fasta_file = "$amino" # Input fasta file
wanted_file = "multi_headers.txt"# Input interesting sequence IDs, one per line
result_file = "CompleteMultiExonics.faa" # Output fasta file

wanted = set()
with open(wanted_file) as f:
    for line in f:
        line = line.strip()
        if line != "":
            wanted.add(line)

fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')
with open(result_file, "w") as f:
    for seq in fasta_sequences:
        if seq.id in wanted:
            SeqIO.write([seq], f, "fasta")
EOM
echo "file: echo grabfiles.py created"
### run python script
/bin/cat <<EOM >"./run_grabfiles.sh"
#!/bin/bash
#SBATCH --job-name=grabfile
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grab_%j.out
#SBATCH -e grab_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
python grabfiles.py

EOM
echo "file: run_grabfiles.sh created"
#### run entap.sh with two -d flags
### Create entap folder and run file
#### coverage variables can be changed here

mkdir entap_out

/bin/cat <<EOM >"./multi_entap.sh"
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=128G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
echo hostname

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP \\
-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \\
-d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd \\
--i CompleteMultiExonics.faa \\
--threads 16 \\
--tcoverage 50 \\
--qcoverage 50 \\
--out-dir ./entap_out
EOM
echo "file: multi_entap.sh created"

### Run all of the inputs
#### sbatch spits out a full line of text "Submitted job id (number)", in order to extract the job id, cut that output by spaces and grab the fourth number. May need to change if slurm output changes for job confirmations. 

job1=$(sbatch multi_gFACs.sh)
jid1=$(echo "$job1" | cut -d ' ' -f 4)
job2=$(sbatch --dependency=afterok:$jid1 multi_grep_headers.sh)
jid2=$(echo "$job2" | cut -d ' ' -f 4)
job3=$(sbatch --dependency=afterok:$jid2 run_grabfiles.sh)
jid3=$(echo "$job3" | cut -d ' ' -f 4)
sbatch --dependency=afterok:$jid3 multi_entap.sh
echo "all *.sh files queued in slurm system"

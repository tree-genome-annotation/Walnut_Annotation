#!/bin/bash

#Usage: 'sh CCmono.sh email genome_fasta augustus.gff3'
# Inputs must be full paths
# A gFACs folder must exist in the directory this script runs in, named "gFACs" and containing "gFACs.pl"
email=$1
fasta=$PWD/$2
gff3=$PWD/$3
amino=$PWD/$4

echo "Full input file paths:"
echo $fasta
echo $gff3
echo $amino

echo "Your email: $email"
mkdir mono_out

#### Create gFACs run file

gFACsFILE="./mono_gFACs.sh"
/bin/cat <<EOM >$gFACsFILE
#!/bin/bash
#BATCH --job-name=gFACs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --mem=32GB
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
#SBATCH --mail-type=END
#SBATCH --mail-user=$email
#SBATCH -o stats_%j.o
#SBATCH -e stats_%j.e

module load perl
cd gFACs/

perl gFACs.pl \\
-f braker_2.0_gff3 \\
--statistics \\
--statistics-at-every-step \\
-p Mono \\
--splice-rescue \\
--rem-start-introns \\
--rem-multiexonics \\
--rem-end-introns \\
--splice-table \\
--min-exon-size 20 \\
--min-intron-size 20 \\
--rem-genes-without-start-codon \\
--rem-genes-without-stop-codon \\
--get-fasta-without-introns \\
--get-fasta-with-introns \\
--create-gtf \\
--fasta $fasta \\
-O ../mono_out \\
$gff3
EOM

##### Create gFACs check script
#
##statsCHECK="./check_gFACs.sh"
##bin/cat <<EOM >$gFACsCHECK
##!/bin/bash
##SBATCH --job-name=gstatcheck
##SBATCH --mail-user=$email
##SBATCH --mail-type=END
##SBATCH -n 1
##SBATCH -N 1
##SBATCH -c 1
##SBATCH --mem=8G
##SBATCH -o gsc_%j.out
##SBATCH -e gsc_%j.err
##SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
#
##ase=$(pwd)
#
##d $base/mono_out/
#
##rep -m 1 "Number of genes:" gFACs_log.txt >> $base/gFACscheck.txt
##rep -m 1 "Number of monoexonic genes:" gFACs_log.txt >> $base/gFACscheck.txt
##rep -m 1 "Number of monoexonic genes:" gFACs_log.txt >> $base/gFACscheck.txt
#
##d $base/multi_out/
#
##rep -m 1 "Number of genes:" gFACs_log.txt >> $base/gFACscheck.txt
##rep -m 1 "Number of monoexonic genes:" gFACs_log.txt >> $base/gFACscheck.txt
##rep -m 1 "Number of monoexonic genes:" gFACs_log.txt >> $base/gFACscheck.txt
#
# Good ole command substitution
#Chck="$( uniq $base/gFACscheck | wc -l )"

#if [ $Chck == 3 ]; then
#    GSTATS_CHECK="Good"
#    export GSTATS_CHECK
#else
#    GSTATS_CHECK="Bad"
#    export GSTATS_CHECK
#fi
#EOM

## use headers with grabfiles.py and completeMonoExonics.fasta file
#### must create a bash file so it can be run with slurm job_id dependencies
/bin/cat <<EOM >"./mono_grep_headers.sh"
#!/bin/bash
#SBATCH --job-name=grep
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grep_%j.out
#SBATCH -e grep_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

grep ">" mono_out/Mono_genes_with_introns.fasta | grep -oE "g.*" > mono_headers.txt
sed -i -e 's/\(g[[:digit:]]*\)\.\([[:digit:]]*\)/\1\.t\2/g' mono_headers.txt
sed -i -e 's/\(g[[:digit:]]*\)$/\1.t1/g' mono_headers.txt
EOM
#### Pipe grep into grep to omit leading `>` character

### Create grabfiles.py and assign inputs

grabpy="./grabfilesmono1.py"
/bin/cat <<EOM >$grabpy
from Bio import SeqIO

fasta_file = "$amino" # Input fasta file
wanted_file = "mono_headers.txt"# Input interesting sequence IDs, one per line
result_file = "CompleteMonoExonics.faa" # Output fasta file

wanted = set()
with open(wanted_file) as f:
    for line in f:
        line = line.strip()
        if line != "":
            wanted.add(line)

fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')
with open(result_file, "w") as f:
    for seq in fasta_sequences:
        if seq.id in wanted:
            SeqIO.write([seq], f, "fasta")
EOM

### run python script

/bin/cat <<EOM >"./run_grabfiles.sh"
#!/bin/bash
#SBATCH --job-name=grabmonos
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grabmonos_%j.out
#SBATCH -e grabmonos_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
python grabfilesmono1.py

EOM


#### Split the GeeeeNomeeee!

splitfipy="./splitfasta.py"
/bin/cat <<EOM >$splitfipy
from Bio import SeqIO
import argparse
import numpy as np
import os

parser = argparse.ArgumentParser(
     prog='splitfasta2.py',
     usage='''python splitfasta2.py --fasta [Genome fasta file] --path [Path of genome file] --pieces [No. of pieces desired] --pathOut [direct the output]''',
     description='''This program splits a fasta sequence into several similarly-sized pieces.''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)
parser.add_argument('--path', type=str, help='The path of the fasta file', required=False)
parser.add_argument('--pieces', type=int, help='No. of pieces desired', required=True)
parser.add_argument('--pathOut', type=str, help='path of output files', required=False)

args = parser.parse_args()
pathOut = args.pathOut
filename1=args.fasta
path=args.path
pieces=args.pieces
if path==None:
  filename2=open(filename1,'r')
else:
  filename2=open(os.path.join(path, filename1), "r")

seq_records1 = SeqIO.parse(filename2, "fasta")
seq_records1 = list(seq_records1)

# Calulate total number of bases

genome=0

for seq_record in seq_records1:
   genome+=len(seq_record.seq)

splice=genome/pieces
j=0
for i in range(pieces):
    if pathOut==None:
      c = filename1 + str(i+1) + ".fa"
    else:
      c = pathOut + filename1 + str(i+1) + ".fa"
    breaks=0
    with open(c,'w') as ijk:
         while (breaks < splice) and j < len(seq_records1):
             ijk.write("%s%s\n%s\n" % (">",seq_records1[j].id, seq_records1[j].seq)) # Making the fasta file
             breaks+=len(seq_records1[j].seq)
             j+=1
    ijk.close()
EOM

#### OK, now actually split the genome

/bin/cat <<EOM >"./splitfasta.sh"
#!/bin/bash
#SBATCH --job-name=grabfile
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grab_%j.out
#SBATCH -e grab_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
mkdir splitgenome
python splitfasta.py --fasta CompleteMonoExonics.faa --pieces 50 --pathOut splitgenome/split

EOM


#### Run CerealInnerPrough

cereal="./serial_interpro.sh"
/bin/cat <<EOM >$cereal
#!/bin/bash
##Submission script for Xanadu
###SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=SerialInterPro
#SBATCH -o interpro-%j.output
#SBATCH -e interpro-%j.error
#SBATCH --mail-user=$email
#SBATCH --mail-type=ALL
#SBATCH --array=1-50%4
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=64G
#SBATCH --partition=amd

module load interproscan/5.25-64.0                                                                                                    
mkdir interproresults

echo splitCompleteMonoExonicProteins.faa\$SLURM_ARRAY_TASK_ID.fa

interproscan.sh -i ./splitgenome/splitCompleteMonoExonics.faa\$SLURM_ARRAY_TASK_ID.fa -o interproresults/splitMonoExonic\$SLURM_ARRAY_TASK_ID.gff3 -f gff3 -appl Pfam -goterms -iprlookup
EOM

### Python script to get headers that have passed InterProScan



### Concatenate GFF3 outfiles from SerialInterproscan
catcereal="./cat_interproResults.sh"
/bin/cat <<EOM >$catcereal
#!/bin/bash
#SBATCH --job-name=grabfile
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o catIPS_%j.out
#SBATCH -e catIPS_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

cat interproresults/splitMonoExonic*.gff3 > interprofiltered.gff3
grep "##sequence-region" interprofiltered.gff3 > interprofilt1.txt
awk '{print \$2}' interprofilt1.txt > monoheaders_IPSfilt.txt
sed -i /^$/d monoheaders_IPSfilt.txt
EOM



### Create grabfiles.py and assign inputs

grabpy2="./grabfilesmono2.py"
/bin/cat <<EOM >$grabpy2
from Bio import SeqIO

fasta_file = "CompleteMonoExonics.faa" # Input fasta file
wanted_file = "monoheaders_IPSfilt.txt"# Input interesting sequence IDs, one per line
result_file = "MonoIPSfilt.faa" # Output fasta file

wanted = set()
with open(wanted_file) as f:
    for line in f:
        line = line.strip()
        if line != "":
            wanted.add(line)

fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')
with open(result_file, "w") as f:
    for seq in fasta_sequences:
        if seq.id in wanted:
            SeqIO.write([seq], f, "fasta")
EOM

### run python script

/bin/cat <<EOM >"./run_grabfiles2.sh"
#!/bin/bash
#SBATCH --job-name=grabfilei2
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grab_%j.out
#SBATCH -e grab_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
python grabfilesmono2.py

EOM

### Run enTAP on the new filtered amino acid file

/bin/cat <<EOM >"./monoentap.sh"
#!/bin/bash
#SBATCH --job-name=mono_entap
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=120G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP \\
-d /UCHC/LABS/Wegrzyn/WalnutGenomes/MonoExonicDbProteins.dmnd \\
--i MonoIPSfilt.faa \\
--threads 12 \\
--tcoverage 80 \\
--qcoverage 80 \\
--out-dir ./monoentap_out
EOM

### Get list of headers that have annotations: The Python file!

Subseq="./get_Subseq.py"
/bin/cat <<EOM >$Subseq

with open("monoentap_out/final_annotations_lvl0_no_contam.tsv") as f:
    lines = f.readlines()

diamond = 0
eggnog = 0

for line in lines[1:]:
    splines = line.split("\t")
    subseq = splines[1]
    if subseq != "":
        print line
        diamond += 1
    else:
        #print "Yahh"
        Nog = splines[18]
        #print Nog
        if Nog != "":
            print line
            eggnog += 1
EOM

#### Run that Python script

/bin/cat <<EOM >"./Subseq.sh"
#!/bin/bash
#SBATCH --job-name=subseqs
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o subseq_%j.out
#SBATCH -e subseq_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

python get_Subseq.py > prefinalmonoheaders.txt

awk '{print \$1}' prefinalmonoheaders.txt > finalmonoheaders.txt

EOM


## Create grabfiles.py and assign inputs

grabpy3="./grabfilesmono3.py"
/bin/cat <<EOM >$grabpy3
from Bio import SeqIO

fasta_file = "MonoIPSfilt.faa" # Input fasta file
wanted_file = "finalmonoheaders.txt"# Input interesting sequence IDs, one per line
result_file = "Mono_final.faa" # Output fasta file

wanted = set()
with open(wanted_file) as f:
    for line in f:
        line = line.strip()
        if line != "":
            wanted.add(line)

fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')
with open(result_file, "w") as f:
    for seq in fasta_sequences:
        if seq.id in wanted:
            SeqIO.write([seq], f, "fasta")
EOM

### run python script

/bin/cat <<EOM >"./run_grabfiles3.sh"
#!/bin/bash
#SBATCH --job-name=grabfile3
#SBATCH --mail-user=$email
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grab_%j.out
#SBATCH -e grab_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
python grabfilesmono3.py

EOM

### Create a subset monoexonics.gtf from the out.gtf of gFACs


#un all of the inputs
#### sbatch spits out a full line of text "Submitted job id (number)", in order to extract the job id, cut that output by spaces and grab the fourth number. May need to change if slurm output changes for job confirmations. 

job1=$(sbatch mono_gFACs.sh)
jid1=$(echo "$job1" | cut -d ' ' -f 4)
job2=$(sbatch --dependency=afterok:$jid1 mono_grep_headers.sh)
jid2=$(echo "$job2" | cut -d ' ' -f 4)
job3=$(sbatch --dependency=afterok:$jid2 run_grabfiles.sh)
jid3=$(echo "$job3" | cut -d ' ' -f 4)
job4=$(sbatch --dependency=afterok:$jid3 splitfasta.sh)
jid4=$(echo "$job4" | cut -d ' ' -f 4)
job5=$(sbatch --dependency=afterok:$jid4 serial_interpro.sh)
jid5=$(echo "$job5" | cut -d ' ' -f 4)
job6=$(sbatch --dependency=afterok:$jid5 cat_interproResults.sh)
jid6=$(echo "$job6" | cut -d ' ' -f 4)
job7=$(sbatch --dependency=afterok:$jid6 run_grabfiles2.sh)
jid7=$(echo "$job7" | cut -d ' ' -f 4)
job8=$(sbatch --dependency=afterok:$jid7 monoentap.sh)
jid8=$(echo "$job8" | cut -d ' ' -f 4)
job9=$(sbatch --dependency=afterok:$jid8 Subseq.sh)
jid9=$(echo "$job9" | cut -d ' ' -f 4)
sbatch --dependency=afterok:$jid9 run_grabfiles3.sh


#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=taylor.falk@uconn.edu
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
echo hostname

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd --i /UCHC/LABS/Wegrzyn/WalnutGenomes/IronWal/postbraker/neo-entap/Iron_gstats_aa_out.faa --threads 4 --tcoverage 50 --qcoverage 50 --out-dir /UCHC/LABS/Wegrzyn/WalnutGenomes/IronWal/postbraker/neo-entap/.

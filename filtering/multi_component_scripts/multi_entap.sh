#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=taylor.falk3@gmail.com
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=128GG
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
echo hostname

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP \
-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
-d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd \
--i CompleteMultiExonics.faa \
--threads 16 \
--tcoverage 50 \
--qcoverage 50 \
--out-dir ./entap_out

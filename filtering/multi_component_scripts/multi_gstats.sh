#!/bin/bash
#BATCH --job-name=gstats
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --mem=32GB
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
#SBATCH --mail-type=END
#SBATCH --mail-user=taylor.falk3@gmail.com
#SBATCH -o stats_%j.o
#SBATCH -e stats_%j.e

module load perl
cd gstats/

perl gstats.pl \
-f braker_2.0_gff3 \
--statistics \
--statistics-at-every-step \
-p Multi \
--splice-rescue \
--rem-start-introns \
--rem-monoexonics \
--rem-end-introns \
--splice-table \
--min-exon-size 20 \
--min-intron-size 20 \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--get-fasta-without-introns \
--get-fasta-with-introns \
--create-gtf \
--fasta /UCHC/LABS/Wegrzyn/WalnutGenomes/git_walnut_scripts/combined_script/multi_pipeline/secondFilteredMaskedIron.fa \
-O ../multi_out \
/UCHC/LABS/Wegrzyn/WalnutGenomes/git_walnut_scripts/combined_script/multi_pipeline/augustus.gff3

#!/bin/bash
#BATCH --job-name=gstats
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --mem=32GB
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5
#SBATCH --mail-type=END
#SBATCH --mail-user=taylor.falk@uconn.edu
#SBATCH -o stats_%j.o
#SBATCH -e stats_%j.e

module load perl

perl gstats.pl \
-f braker_2.0_gff3 \
--statistics \
--statistics-at-every-step \
-p Multi \
--splice-rescue \
--rem-start-introns \
--rem-monoexonics \
--rem-end-introns \
--splice-table \
--min-exon-size 20 \
--min-intron-size 20 \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--get-fasta-without-introns \
--get-fasta-with-introns \
--create-gtf \
--fasta /UCHC/LABS/Wegrzyn/WalnutGenomes/IronWal/secondFilteredMaskedIron.fa \
-O /UCHC/LABS/Wegrzyn/WalnutGenomes/IronWal/postbraker/gstats/iron_out \
/UCHC/LABS/Wegrzyn/WalnutGenomes/IronWal/postbraker/augustus.gff3

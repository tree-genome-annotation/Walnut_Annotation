#!/bin/bash
#SBATCH --job-name=grabmonos
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grabmonos_%j.out
#SBATCH -e grabmonos_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
python grabfilesmono1.py


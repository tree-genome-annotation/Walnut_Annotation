#!/bin/bash
#SBATCH --job-name=grep
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grep_%j.out
#SBATCH -e grep_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

grep ">" mono_out/Mono_genes_with_introns.fasta | grep -oE "g.*" > mono_headers.txt
sed -i -e 's/\(g[[:digit:]]*\)\.\([[:digit:]]*\)/\1\.t\2/g' mono_headers.txt
sed -i -e 's/\(g[[:digit:]]*\)$/\1.t1/g' mono_headers.txt

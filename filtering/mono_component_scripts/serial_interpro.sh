#!/bin/bash
##Submission script for Xanadu
###SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=SerialInterPro
#SBATCH -o interpro-%j.output
#SBATCH -e interpro-%j.error
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH --array=1-50%4
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=64G
#SBATCH --partition=amd

module load interproscan/5.25-64.0                                                                                                    
mkdir interproresults

echo splitCompleteMonoExonicProteins.faa$SLURM_ARRAY_TASK_ID.fa

interproscan.sh -i ./splitgenome/splitCompleteMonoExonics.faa$SLURM_ARRAY_TASK_ID.fa -o interproresults/splitMonoExonic$SLURM_ARRAY_TASK_ID.gff3 -f gff3 -appl Pfam -goterms -iprlookup

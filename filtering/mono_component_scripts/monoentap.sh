#!/bin/bash
#SBATCH --job-name=mono_entap
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=120G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP \
-d /UCHC/LABS/Wegrzyn/WalnutGenomes/MonoExonicDbProteins.dmnd \
--i MonoIPSfilt.faa \
--threads 12 \
--tcoverage 80 \
--qcoverage 80 \
--out-dir ./monoentap_out

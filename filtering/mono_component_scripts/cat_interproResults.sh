#!/bin/bash
#SBATCH --job-name=grabfile
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o catIPS_%j.out
#SBATCH -e catIPS_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

cat interproresults/splitMonoExonic*.gff3 > interprofiltered.gff3
grep "##sequence-region" interprofiltered.gff3 > interprofilt1.txt
awk '{print $2}' interprofilt1.txt > monoheaders_IPSfilt.txt
sed -i /^$/d monoheaders_IPSfilt.txt

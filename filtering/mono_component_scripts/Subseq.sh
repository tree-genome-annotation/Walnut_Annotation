#!/bin/bash
#SBATCH --job-name=subseqs
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o subseq_%j.out
#SBATCH -e subseq_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

python get_Subseq.py > prefinalmonoheaders.txt

awk '{print $1}' prefinalmonoheaders.txt > finalmonoheaders.txt


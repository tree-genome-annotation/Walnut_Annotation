#!/bin/bash
#SBATCH --job-name=grabfile
#SBATCH --mail-user=
#SBATCH --mail-type=END
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o grab_%j.out
#SBATCH -e grab_%j.err
#SBATCH --partition=general,himem1,himem2,himem3,himem4,himem5

module load anaconda2/4.4.0
mkdir splitgenome
python splitfasta.py --fasta CompleteMonoExonics.faa --pieces 50 --pathOut splitgenome/split

